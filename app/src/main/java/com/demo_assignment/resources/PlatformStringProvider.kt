package com.demo_assignment.resources

import android.app.Application
import javax.inject.Inject

interface PlatformStringProvider : StringProvider<Int>

class PlatformStringProviderImpl
@Inject constructor(
  private val application: Application
) : PlatformStringProvider {

  override fun getString(id: Int, vararg args: Any): String =
    if (args.isEmpty()) {
      application.getString(id)
    } else {
      String.format(application.getString(id), *args)
    }
}