package com.demo_assignment.resources

interface StringProvider<Identifier> {
  fun getString(id: Identifier, vararg args: Any): String
}

