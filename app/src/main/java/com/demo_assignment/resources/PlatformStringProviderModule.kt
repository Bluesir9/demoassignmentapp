package com.demo_assignment.resources

import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class PlatformStringProviderModule {

  @Binds
  @Singleton
  abstract fun provideStringProvider(impl: PlatformStringProviderImpl): PlatformStringProvider
}