package com.demo_assignment.ui.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun LifecycleOwner.repeatWhenCreated(block: suspend CoroutineScope.() -> Unit) {
  this.lifecycleScope.launch {
    this@repeatWhenCreated.lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED, block)
  }
}