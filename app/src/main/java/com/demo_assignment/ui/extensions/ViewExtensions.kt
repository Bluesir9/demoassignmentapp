package com.demo_assignment.ui.extensions

import android.view.View
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

fun View.setVisibility(visible: Boolean) {
  this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.getClicks(): Flow<Unit> =
  callbackFlow {
    val listener = View.OnClickListener { offer(Unit) }
    setOnClickListener(listener)
    awaitClose { setOnClickListener(null) }
  }