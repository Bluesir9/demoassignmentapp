package com.demo_assignment.ui.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.demo_assignment.ui.error.FragmentCentralErrorRenderer
import com.demo_assignment.ui.progress.FragmentCentralProgressBarRenderer
import com.demo_assignment.url_navigator.FragmentUrlNavigator
import javax.inject.Inject

abstract class BaseFragment(@LayoutRes contentLayoutId: Int): Fragment(contentLayoutId) {

  @Inject
  lateinit var centralErrorRenderer: FragmentCentralErrorRenderer
  @Inject
  lateinit var centralProgressBarRenderer: FragmentCentralProgressBarRenderer
  @Inject
  lateinit var urlNavigator: FragmentUrlNavigator

  override fun onAttach(context: Context) {
    inject()
    super.onAttach(context)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    centralErrorRenderer.begin()
    centralProgressBarRenderer.begin()
    urlNavigator.begin()
  }

  abstract fun inject()
}