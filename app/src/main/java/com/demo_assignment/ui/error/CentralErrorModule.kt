package com.demo_assignment.ui.error

import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.Module
import dagger.Provides

@Module
class CentralErrorModule {

  /*
  Using `.value` is safe cause the ViewModel doesn't use SavedStateHandle, so injecting in
  `onCreate` would work fine.
  */
  @Provides
  fun centralErrorViewModel(activity: AppCompatActivity): CentralErrorViewModel =
    activity.viewModels<CentralErrorViewModel>().value

  @Provides
  fun centralError(viewModel: CentralErrorViewModel): CentralError = viewModel
}