package com.demo_assignment.ui.error

data class CentralErrorUiModel(
  val message: String,
  val buttonText: String
)