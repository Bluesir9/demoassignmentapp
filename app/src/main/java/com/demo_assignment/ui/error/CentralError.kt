package com.demo_assignment.ui.error

import kotlinx.coroutines.flow.Flow

interface CentralError {
  val errorButtonClickEvents: Flow<Unit>
  fun show(message: String, buttonText: String)
  fun hide()
}