package com.demo_assignment.ui.error

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Hidden
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Visible
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class CentralErrorViewModel @Inject constructor(): ViewModel(), CentralError {

  private val mutableCentralErrorUiModels =
    MutableStateFlow<VisibleOrHiddenUiModel<CentralErrorUiModel>>(Hidden)
  val centralErrorUiModels: Flow<VisibleOrHiddenUiModel<CentralErrorUiModel>> =
    mutableCentralErrorUiModels

  private val mutableErrorButtonClickEvents = MutableSharedFlow<Unit>()
  override val errorButtonClickEvents: Flow<Unit> = mutableErrorButtonClickEvents

  fun onErrorButtonClick() {
    viewModelScope.launch { mutableErrorButtonClickEvents.emit(Unit) }
  }

  override fun show(message: String, buttonText: String) {
    val uiModel = CentralErrorUiModel(
      message = message,
      buttonText = buttonText
    )
    viewModelScope.launch {
      mutableCentralErrorUiModels.emit(Visible(uiModel))
    }
  }

  override fun hide() {
    viewModelScope.launch { mutableCentralErrorUiModels.emit(Hidden) }
  }
}