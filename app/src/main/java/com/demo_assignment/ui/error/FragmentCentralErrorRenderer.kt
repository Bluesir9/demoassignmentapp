package com.demo_assignment.ui.error

import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.demo_assignment.R
import com.demo_assignment.ui.extensions.getClicks
import com.demo_assignment.ui.extensions.repeatWhenCreated
import com.demo_assignment.ui.extensions.setVisibility
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class FragmentCentralErrorRenderer @Inject constructor(
  private val fragment: Fragment,
  private val viewModel: CentralErrorViewModel
) {

  private val centralErrorText: TextView by lazy {
    fragment.requireView().findViewById(R.id.centralErrorText)
  }
  private val centralErrorButton: Button by lazy {
    fragment.requireView().findViewById(R.id.centralErrorButton)
  }

  fun begin() {
    setupErrorButtonClickEvents()
    listenToUiModels()
  }

  private fun setupErrorButtonClickEvents() {
    fragment.repeatWhenCreated {
      centralErrorButton.getClicks().collect { viewModel.onErrorButtonClick() }
    }
  }

  private fun listenToUiModels() {
    fragment.repeatWhenCreated {
      viewModel.centralErrorUiModels.collect { render(it) }
    }
  }

  private fun render(uiModel: VisibleOrHiddenUiModel<CentralErrorUiModel>) {
    centralErrorText.setVisibility(uiModel.visible)
    centralErrorButton.setVisibility(uiModel.visible)
    if(uiModel is VisibleOrHiddenUiModel.Visible) {
      centralErrorText.text = uiModel.model.message
      centralErrorButton.text = uiModel.model.buttonText
    }
  }
}