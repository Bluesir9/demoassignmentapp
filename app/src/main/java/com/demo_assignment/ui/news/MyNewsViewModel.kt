package com.demo_assignment.ui.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo_assignment.logger.Logger
import com.demo_assignment.model.News
import com.demo_assignment.news.MyNewsRepository
import com.demo_assignment.resources.StringProvider
import com.demo_assignment.ui.error.CentralError
import com.demo_assignment.ui.news.MyNewsString.FailedToLoadNews
import com.demo_assignment.ui.news.MyNewsString.TryAgain
import com.demo_assignment.ui.progress.CentralProgressBar
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Hidden
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Visible
import com.demo_assignment.url_navigator.UrlNavigator
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MyNewsViewModel(
  private val repository: MyNewsRepository,
  private val logger: Logger,
  private val centralError: CentralError,
  private val centralProgressBar: CentralProgressBar,
  private val stringProvider: StringProvider<MyNewsString>,
  private val urlNavigator: UrlNavigator
) : ViewModel() {

  private val logTag = MyNewsViewModel::class.simpleName!!

  private var newsLoadJob: Job? = null
  private val mutableNewsItems = MutableStateFlow<VisibleOrHiddenUiModel<List<News>>>(Hidden)
  val newsItems: Flow<VisibleOrHiddenUiModel<List<News>>> = mutableNewsItems

  init {
    listenToTryAgainClicks()
    loadNews()
  }

  private fun listenToTryAgainClicks() {
    viewModelScope.launch { centralError.errorButtonClickEvents.collect { onTryAgainClick() } }
  }

  private fun onTryAgainClick() {
    centralError.hide()
    loadNews()
  }

  private fun loadNews() {
    /*
    An API call is already running, so don't start another one.
    */
    if(newsLoadJob?.isActive == true) {
      return
    }

    viewModelScope.launch {
      try {
        centralProgressBar.show()
        val myNews = repository.getMyNews()
        mutableNewsItems.emit(Visible(myNews))
      } catch (e: Exception) {
        logger.e(logTag, "failed to load news", e)
        centralError.show(
          message = stringProvider.getString(FailedToLoadNews),
          buttonText = stringProvider.getString(TryAgain)
        )
      } finally {
        centralProgressBar.hide()
      }
    }
  }

  fun onNewsItemClick(news: News) {
    urlNavigator.open(news.newsLink)
  }
}