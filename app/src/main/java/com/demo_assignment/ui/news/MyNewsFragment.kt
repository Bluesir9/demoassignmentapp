package com.demo_assignment.ui.news

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.RecyclerView
import com.demo_assignment.R
import com.demo_assignment.model.News
import com.demo_assignment.ui.base.BaseFragment
import com.demo_assignment.ui.extensions.setVisibility
import com.demo_assignment.ui.main_activity.MainActivity
import com.demo_assignment.ui.news.di.MyNewsFragmentSubComponent
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MyNewsFragment : BaseFragment(R.layout.fragment_my_news) {

  private lateinit var subComponent: MyNewsFragmentSubComponent

  @Inject
  lateinit var myNewsViewModelFactory: MyNewsViewModelFactory
  private val viewModel: MyNewsViewModel by viewModels { myNewsViewModelFactory }

  private val recyclerView: RecyclerView by lazy {
    val view = requireView().findViewById<RecyclerView>(R.id.newsRecyclerView)
    view.adapter = NewsAdapter(clickListener = { viewModel.onNewsItemClick(it) })
    view
  }

  override fun inject() {
    subComponent =
      (activity as MainActivity)
        .subComponent
        .plus(MyNewsFragmentModule(this))
    subComponent.inject(this)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    listenToNewsItems()
  }

  private fun listenToNewsItems() {
    lifecycleScope.launch {
      lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
        viewModel.newsItems.collect { render(it) }
      }
    }
  }

  private fun render(uiModel: VisibleOrHiddenUiModel<List<News>>) {
    recyclerView.setVisibility(uiModel.visible)
    if (uiModel is Visible) {
      render(uiModel.model)
    }
  }

  private fun render(newsItems: List<News>) {
    (recyclerView.adapter as NewsAdapter).setNewsItems(newsItems)
  }
}
