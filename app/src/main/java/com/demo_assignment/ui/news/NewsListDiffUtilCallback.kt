package com.demo_assignment.ui.news

import androidx.recyclerview.widget.DiffUtil
import com.demo_assignment.model.News

class NewsListDiffUtilCallback(
  private val oldList: List<News>,
  private val newList: List<News>
) : DiffUtil.Callback() {

  override fun getOldListSize(): Int = oldList.size

  override fun getNewListSize(): Int = newList.size

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
    oldList[oldItemPosition].newsLink == newList[newItemPosition].newsLink

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
    oldList[oldItemPosition] == newList[newItemPosition]
}