package com.demo_assignment.ui.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo_assignment.logger.Logger
import com.demo_assignment.news.MyNewsRepository
import com.demo_assignment.resources.StringProvider
import com.demo_assignment.ui.error.CentralError
import com.demo_assignment.ui.progress.CentralProgressBar
import com.demo_assignment.url_navigator.UrlNavigator
import javax.inject.Inject

class MyNewsViewModelFactory @Inject constructor(
  private val repository: MyNewsRepository,
  private val logger: Logger,
  private val centralError: CentralError,
  private val centralProgressBar: CentralProgressBar,
  private val stringProvider: StringProvider<MyNewsString>,
  private val urlNavigator: UrlNavigator
) : ViewModelProvider.Factory {

  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    return MyNewsViewModel(
      repository = repository,
      logger = logger,
      centralError = centralError,
      centralProgressBar = centralProgressBar,
      stringProvider = stringProvider,
      urlNavigator = urlNavigator
    ) as T
  }
}