package com.demo_assignment.ui.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.demo_assignment.R
import com.demo_assignment.model.News

class NewsAdapter(
    private val clickListener: (news: News) -> Unit
) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private val newsItems = ArrayList<News>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return NewsViewHolder(view)
    }

    override fun getItemCount() = newsItems.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = newsItems[position]
        holder.render(news)
    }

    fun setNewsItems(newListOfNewsItems: List<News>) {
        val callback = NewsListDiffUtilCallback(newsItems, newListOfNewsItems)
        val diffResult = DiffUtil.calculateDiff(callback)
        newsItems.clear()
        newsItems.addAll(newListOfNewsItems)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val titleView: TextView = itemView.findViewById(R.id.news_title)
        private val newsView: ImageView = itemView.findViewById(R.id.news_view)
        private val resourceImage: ImageView = itemView.findViewById(R.id.resource_icon)
        private val resourceName: TextView = itemView.findViewById(R.id.resource_name)

        fun render(news: News) {
            titleView.text = news.title
            newsView.load(url = news.imageURL)
            resourceImage.load(url = news.resourceURL)
            resourceName.text = news.resourceName
            itemView.setOnClickListener { clickListener(news) }
        }
    }
}