package com.demo_assignment.ui.news

import androidx.fragment.app.Fragment
import com.demo_assignment.ui.news.di.MyNewsFragmentSubComponent
import dagger.Module
import dagger.Provides

@Module
class MyNewsFragmentModule(private val fragment: MyNewsFragment) {

  @Provides
  @MyNewsFragmentSubComponent.Scope
  fun fragment(): Fragment = fragment
}