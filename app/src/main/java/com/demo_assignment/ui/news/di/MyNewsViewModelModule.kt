package com.demo_assignment.ui.news.di

import com.demo_assignment.resources.StringProvider
import com.demo_assignment.ui.news.MyNewsString
import com.demo_assignment.ui.news.MyNewsStringProvider
import dagger.Binds
import dagger.Module

@Module
abstract class MyNewsViewModelModule {

  @Binds
  @MyNewsFragmentSubComponent.Scope
  abstract fun myNewsStringProvider(impl: MyNewsStringProvider): StringProvider<MyNewsString>
}