package com.demo_assignment.ui.news

import com.demo_assignment.R
import com.demo_assignment.resources.PlatformStringProvider
import com.demo_assignment.resources.StringProvider
import com.demo_assignment.ui.news.MyNewsString.*
import javax.inject.Inject

enum class MyNewsString {
  FailedToLoadNews,
  TryAgain
}

class MyNewsStringProvider @Inject constructor(
  private val platformStringProvider: PlatformStringProvider
): StringProvider<MyNewsString> {

  override fun getString(id: MyNewsString, vararg args: Any): String {
    val resourceId =
      when(id) {
        FailedToLoadNews -> R.string.failedToLoadNews
        TryAgain -> R.string.tryAgain
      }
    return platformStringProvider.getString(resourceId, args)
  }
}