package com.demo_assignment.ui.news.di

import com.demo_assignment.news.MyNewsDataModule
import com.demo_assignment.ui.error.CentralErrorModule
import com.demo_assignment.ui.news.MyNewsFragment
import com.demo_assignment.ui.news.MyNewsFragmentModule
import com.demo_assignment.ui.progress.CentralProgressBarModule
import com.demo_assignment.url_navigator.UrlNavigatorModule
import dagger.Subcomponent

@MyNewsFragmentSubComponent.Scope
@Subcomponent(
  modules = [
    MyNewsFragmentModule::class,
    MyNewsViewModelModule::class,
    CentralProgressBarModule::class,
    CentralErrorModule::class,
    MyNewsDataModule::class,
    UrlNavigatorModule::class
  ]
)
interface MyNewsFragmentSubComponent {
  fun inject(myNewsFragment: MyNewsFragment)

  @javax.inject.Scope
  @Retention(AnnotationRetention.RUNTIME)
  annotation class Scope
}