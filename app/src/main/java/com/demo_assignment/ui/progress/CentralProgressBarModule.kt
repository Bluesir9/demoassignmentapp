package com.demo_assignment.ui.progress

import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.Module
import dagger.Provides

@Module
class CentralProgressBarModule {

  /*
  Using `.value` is safe cause the ViewModel doesn't use SavedStateHandle, so injecting in
  `onCreate` would work fine.
  */
  @Provides
  fun centralProgressBarViewModel(activity: AppCompatActivity): CentralProgressBarViewModel =
    activity.viewModels<CentralProgressBarViewModel>().value

  @Provides
  fun centralProgressBar(viewModel: CentralProgressBarViewModel): CentralProgressBar = viewModel
}