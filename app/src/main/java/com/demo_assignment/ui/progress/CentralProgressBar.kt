package com.demo_assignment.ui.progress

interface CentralProgressBar {
  fun show()
  fun hide()
}