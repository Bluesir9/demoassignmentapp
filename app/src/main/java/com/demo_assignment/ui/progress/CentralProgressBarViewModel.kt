package com.demo_assignment.ui.progress

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class CentralProgressBarViewModel @Inject constructor(): ViewModel(), CentralProgressBar {

  private val mutableProgressBarVisible = MutableStateFlow(false)
  val progressBarVisible: Flow<Boolean> = mutableProgressBarVisible

  override fun show() {
    emit(true)
  }

  private fun emit(visible: Boolean) {
    viewModelScope.launch { mutableProgressBarVisible.emit(visible) }
  }

  override fun hide() {
    emit(false)
  }
}