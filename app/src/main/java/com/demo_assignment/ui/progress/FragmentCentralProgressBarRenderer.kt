package com.demo_assignment.ui.progress

import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.demo_assignment.R
import com.demo_assignment.ui.extensions.repeatWhenCreated
import com.demo_assignment.ui.extensions.setVisibility
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class FragmentCentralProgressBarRenderer
@Inject constructor(
  private val fragment: Fragment,
  private val viewModel: CentralProgressBarViewModel
) {

  private val centralProgressBar: ProgressBar by lazy {
    fragment.requireView().findViewById(R.id.centralProgressBar)
  }

  fun begin() {
    fragment.repeatWhenCreated {
      viewModel.progressBarVisible.collect { centralProgressBar.setVisibility(it) }
    }
  }
}