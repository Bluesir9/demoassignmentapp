package com.demo_assignment.ui.util

sealed class VisibleOrHiddenUiModel<out UiModel> {
  abstract val visible: Boolean

  object Hidden : VisibleOrHiddenUiModel<Nothing>() {
    override val visible: Boolean = false
  }

  data class Visible<out UiModel>(val model: UiModel): VisibleOrHiddenUiModel<UiModel>() {
    override val visible: Boolean = true
  }
}