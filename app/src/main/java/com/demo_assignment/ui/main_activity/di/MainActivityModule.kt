package com.demo_assignment.ui.main_activity.di

import androidx.appcompat.app.AppCompatActivity
import com.demo_assignment.ui.main_activity.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule(private val mainActivity: MainActivity) {
  @Provides
  @MainActivitySubComponent.Scope
  fun appCompatActivity(): AppCompatActivity = mainActivity
}