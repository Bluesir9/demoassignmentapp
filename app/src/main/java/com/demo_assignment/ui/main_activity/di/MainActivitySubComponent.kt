package com.demo_assignment.ui.main_activity.di

import com.demo_assignment.ui.main_activity.MainActivity
import com.demo_assignment.ui.news.MyNewsFragmentModule
import com.demo_assignment.ui.news.di.MyNewsFragmentSubComponent
import dagger.Subcomponent

@MainActivitySubComponent.Scope
@Subcomponent(
  modules = [
    MainActivityModule::class
  ]
)
interface MainActivitySubComponent {
  fun inject(activity: MainActivity)

  fun plus(
    fragmentModule: MyNewsFragmentModule
  ): MyNewsFragmentSubComponent

  @javax.inject.Scope
  @Retention(AnnotationRetention.RUNTIME)
  annotation class Scope
}