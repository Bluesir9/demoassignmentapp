package com.demo_assignment.ui.main_activity

import android.os.Bundle
import com.demo_assignment.DemoAssignmentApp
import com.demo_assignment.R
import com.demo_assignment.ui.base.BaseActivity
import com.demo_assignment.ui.main_activity.di.MainActivityModule
import com.demo_assignment.ui.main_activity.di.MainActivitySubComponent

class MainActivity: BaseActivity() {

  lateinit var subComponent: MainActivitySubComponent

  override fun inject() {
    subComponent =
      (application as DemoAssignmentApp)
        .appComponent
        .plus(MainActivityModule(this))
    subComponent.inject(this)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
  }
}