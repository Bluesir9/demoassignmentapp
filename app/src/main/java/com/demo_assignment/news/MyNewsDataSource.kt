package com.demo_assignment.news

import com.demo_assignment.model.News

interface MyNewsDataSource {
  suspend fun getMyNews(): List<News>
}