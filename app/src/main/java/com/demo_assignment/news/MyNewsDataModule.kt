package com.demo_assignment.news

import dagger.Binds
import dagger.Module

@Module
abstract class MyNewsDataModule {

  /*
  If we were to add a second data source(example: remote data source, local DB based data source
  etc.) then we could use qualifier annotations to differentiate one data source from the other.
  */
  @Binds
  abstract fun localMyNewsDataSource(impl: LocalMyNewsDataSource): MyNewsDataSource

  @Binds
  abstract fun myNewsRepository(impl: MyNewsRepositoryImpl): MyNewsRepository
}