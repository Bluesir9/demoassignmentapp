package com.demo_assignment.news

import com.demo_assignment.assets.Assets
import com.demo_assignment.assets.Assets.Result.*
import com.demo_assignment.logger.Logger
import com.demo_assignment.model.News
import com.demo_assignment.model.NewsDataModel
import com.squareup.moshi.Moshi
import javax.inject.Inject

/*
TODO:
  Given the underlying news.json file is not going to change beneath our feet, we could save(cache)
  the retrieved `List<News>` in a local variable.
  If that value is not set, then we would retrieve the json from the assets and set it before
  returning it.
  If the value is set, then we would immediately return it.
  However in this case we would have to use `ViewModelScoped` or even `Singleton` scope to ensure
  that all consumers of this class are benefited by this cache.
*/
class LocalMyNewsDataSource @Inject constructor(
  private val assets: Assets,
  private val logger: Logger,
  private val moshi: Moshi
) : MyNewsDataSource {

  private val logTag = LocalMyNewsDataSource::class.simpleName!!
  private val localAssetsNewsFileName = "news.json"

  override suspend fun getMyNews(): List<News> =
    when (val result = assets.getAsString(localAssetsNewsFileName)) {
      is Success -> parseJsonString(result.value)
      is Failure -> {
        logger.e(logTag, "Failed to $localAssetsNewsFileName from assets", result.exception)
        throw result.exception
      }
    }

  private fun parseJsonString(jsonString: String): List<News> {
    val newsAdapter = moshi.adapter(NewsDataModel::class.java)
    return newsAdapter.fromJson(jsonString)!!.news
      .map { newsDataItem ->
        News(
          title = newsDataItem.title,
          imageURL = newsDataItem.imageURL,
          resourceName = newsDataItem.resourceName,
          resourceURL = newsDataItem.resourceURL,
          newsLink = newsDataItem.newsLink
        )
      }
  }
}