package com.demo_assignment.news

import com.demo_assignment.model.News
import javax.inject.Inject

class MyNewsRepositoryImpl @Inject constructor(
  private val localMyNewsDataSource: MyNewsDataSource
) : MyNewsRepository {

  override suspend fun getMyNews(): List<News> = localMyNewsDataSource.getMyNews()
}