package com.demo_assignment.serde

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object SerdeModule {

  @Provides
  @Singleton
  fun moshi(): Moshi = Moshi.Builder().build()
}