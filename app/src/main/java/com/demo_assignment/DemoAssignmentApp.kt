package com.demo_assignment

import android.app.Application
import com.demo_assignment.di.AppComponent
import com.demo_assignment.di.AppModule
import com.demo_assignment.di.DaggerAppComponent

class DemoAssignmentApp : Application() {
  lateinit var appComponent: AppComponent

  override fun onCreate() {
    super.onCreate()
    appComponent =
      DaggerAppComponent
        .builder()
        .appModule(AppModule(this))
        .build()
    appComponent.inject(this)
  }
}