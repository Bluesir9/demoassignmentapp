package com.demo_assignment.logger

import android.util.Log
import javax.inject.Inject

class LogCatLogger @Inject constructor(): Logger {

  private val tagPrefix = "DemoAssignmentApp."

  override fun i(tag: String, message: String) {
    Log.i("$tagPrefix$tag", message)
  }

  override fun d(tag: String, message: String) {
    Log.d("$tagPrefix$tag", message)
  }

  override fun e(tag: String, message: String) {
    Log.e("$tagPrefix$tag", message)
  }

  override fun e(tag: String, message: String, t: Throwable) {
    Log.e("$tagPrefix$tag", message, t)
  }
}