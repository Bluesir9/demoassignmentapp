package com.demo_assignment.logger

interface Logger {
  fun i(tag: String, message: String)
  fun d(tag: String, message: String)
  fun e(tag: String, message: String)
  fun e(tag: String, message: String, t: Throwable)
}