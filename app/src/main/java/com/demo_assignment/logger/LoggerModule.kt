package com.demo_assignment.logger

import dagger.Binds
import dagger.Module

@Module
abstract class LoggerModule {

  @Binds
  abstract fun logger(logger: LogCatLogger): Logger
}