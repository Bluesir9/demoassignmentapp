package com.demo_assignment.assets

interface Assets {

  sealed class Result {
    data class Success(val value: String): Result()
    data class Failure(val exception: Exception): Result()
  }

  suspend fun getAsString(fileName: String): Result
}