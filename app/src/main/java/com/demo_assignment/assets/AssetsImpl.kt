package com.demo_assignment.assets

import android.app.Application
import com.demo_assignment.assets.Assets.Result
import com.demo_assignment.coroutines.dispatchers.DispatcherProvider
import kotlinx.coroutines.withContext
import java.nio.charset.Charset
import javax.inject.Inject

class AssetsImpl @Inject constructor(
  private val application: Application,
  private val dispatcherProvider: DispatcherProvider
) : Assets {

  private val assets get() = application.assets

  /*
  About the warnings:

  Since we are wrapping the blocking IO call with an IO Dispatcher, the underlying code
  is guaranteed to not block the UI thread, yet the IDE is complaining. This is a problem
  with the lint checker that has been discussed online and so as long as the IO dispatcher
  is being used correctly, the code is safe to run. 
  */
  override suspend fun getAsString(fileName: String): Result =
    withContext(dispatcherProvider.io) {
      try {
        val inputStream = assets.open(fileName)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        val resultValue = buffer.toString(Charset.defaultCharset())
        Result.Success(resultValue)
      } catch (e: Exception) {
        Result.Failure(e)
      }
    }
}