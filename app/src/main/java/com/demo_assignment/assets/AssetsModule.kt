package com.demo_assignment.assets

import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AssetsModule {

  @Binds
  @Singleton
  abstract fun assets(impl: AssetsImpl): Assets
}