package com.demo_assignment.url_navigator

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import com.demo_assignment.ui.extensions.repeatWhenCreated
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import javax.inject.Inject

class FragmentUrlNavigator @Inject constructor(
  private val fragment: Fragment,
  private val viewModel: UrlNavigatorViewModel
) {

  fun begin() {
    fragment.repeatWhenCreated {
      viewModel.urlStream
        .filterNotNull()
        .collect { open(it) }
    }
  }

  private fun open(url: String) {
    fragment.requireActivity().startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
  }
}