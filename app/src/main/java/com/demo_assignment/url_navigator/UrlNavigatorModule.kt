package com.demo_assignment.url_navigator

import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.Module
import dagger.Provides

@Module
object UrlNavigatorModule {

  @Provides
  fun urlNavigatorViewModel(activity: AppCompatActivity): UrlNavigatorViewModel =
    activity.viewModels<UrlNavigatorViewModel>().value

  @Provides
  fun urlNavigator(viewModel: UrlNavigatorViewModel): UrlNavigator = viewModel
}