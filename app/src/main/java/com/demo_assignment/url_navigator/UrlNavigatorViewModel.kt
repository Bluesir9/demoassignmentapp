package com.demo_assignment.url_navigator

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class UrlNavigatorViewModel @Inject constructor(): ViewModel(), UrlNavigator {

  private val mutableUrlStream = MutableStateFlow<String?>(null)
  val urlStream: Flow<String?> = mutableUrlStream

  override fun open(url: String) {
    viewModelScope.launch { mutableUrlStream.emit(url) }
  }
}