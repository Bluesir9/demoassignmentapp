package com.demo_assignment.url_navigator

interface UrlNavigator {
  fun open(url: String)
}