package com.demo_assignment.di

import android.app.Application
import com.demo_assignment.DemoAssignmentApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: DemoAssignmentApp) {

  @Provides
  @Singleton
  fun app(): Application = app

  @Provides
  @Singleton
  fun myNewsApp(): DemoAssignmentApp = app
}