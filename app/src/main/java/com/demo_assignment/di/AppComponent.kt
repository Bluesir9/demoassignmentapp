package com.demo_assignment.di

import com.demo_assignment.DemoAssignmentApp
import com.demo_assignment.assets.AssetsModule
import com.demo_assignment.coroutines.dispatchers.DispatcherProviderModule
import com.demo_assignment.logger.LoggerModule
import com.demo_assignment.resources.PlatformStringProviderModule
import com.demo_assignment.serde.SerdeModule
import com.demo_assignment.ui.main_activity.di.MainActivityModule
import com.demo_assignment.ui.main_activity.di.MainActivitySubComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    AppModule::class,
    PlatformStringProviderModule::class,
    LoggerModule::class,
    DispatcherProviderModule::class,
    AssetsModule::class,
    SerdeModule::class
  ]
)
interface AppComponent {
  fun inject(app: DemoAssignmentApp)

  fun plus(
    activityModule: MainActivityModule
  ): MainActivitySubComponent
}