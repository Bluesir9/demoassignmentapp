package com.demo_assignment.coroutines.dispatchers

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DispatcherProviderModule {

  @Provides
  @Singleton
  fun defaultDispatcherProvider(): DispatcherProvider = defaultDispatcherProvider
}