package com.demo_assignment.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewsDataModel(
  @Json(name = "news")
  val news: List<Item>
) {
  @JsonClass(generateAdapter = true)
  data class Item(
    @Json(name = "title")
    val title: String,
    @Json(name = "image_url")
    val imageURL: String,
    @Json(name = "resource_name")
    val resourceName: String,
    @Json(name = "resource_url")
    val resourceURL: String,
    @Json(name = "news_link")
    val newsLink: String
  )
}