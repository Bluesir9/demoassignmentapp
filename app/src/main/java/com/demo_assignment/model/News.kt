package com.demo_assignment.model

data class News(
    val title: String,
    val imageURL: String,
    val resourceName: String,
    val resourceURL: String,
    val newsLink: String
)