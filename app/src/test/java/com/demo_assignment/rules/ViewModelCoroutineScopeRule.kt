package com.demo_assignment.rules

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class ViewModelCoroutineScopeRule : TestRule {

  private val coroutineDispatcher = TestCoroutineDispatcher()
  private val coroutineScope = TestCoroutineScope(coroutineDispatcher)

  override fun apply(base: Statement, description: Description): Statement = object : Statement() {
    override fun evaluate() {
      Dispatchers.setMain(coroutineDispatcher)
      base.evaluate()
      Dispatchers.resetMain()
      coroutineScope.cleanupTestCoroutines()
    }
  }
}