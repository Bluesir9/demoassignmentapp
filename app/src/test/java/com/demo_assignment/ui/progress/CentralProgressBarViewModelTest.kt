package com.demo_assignment.ui.progress

import com.demo_assignment.rules.ViewModelCoroutineScopeRule
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CentralProgressBarViewModelTest {

  @get:Rule
  val viewModelCoroutineScopeRule = ViewModelCoroutineScopeRule()

  private lateinit var centralProgressBarViewModel: CentralProgressBarViewModel

  @Before
  fun setup() {
    centralProgressBarViewModel = CentralProgressBarViewModel()
  }

  @Test
  fun initiallyCentralProgressBarIsNotVisible() = runBlocking {
    assertFalse(centralProgressBarViewModel.progressBarVisible.first())
  }

  @Test
  fun onShow_CentralProgressBarShouldBecomeVisible() = runBlocking {
    val job = launch(start = CoroutineStart.UNDISPATCHED) {
      centralProgressBarViewModel.show()
    }
    val visible = centralProgressBarViewModel.progressBarVisible.first()
    job.cancel()
    assertTrue(visible)
  }

  @Test
  fun onHide_CentralProgressBarShouldBecomeHidden() = runBlocking {
    val job = launch(start = CoroutineStart.UNDISPATCHED) {
      centralProgressBarViewModel.hide()
    }
    val visible = centralProgressBarViewModel.progressBarVisible.first()
    job.cancel()
    assertFalse(visible)
  }
}