package com.demo_assignment.ui.news

import com.demo_assignment.logger.Logger
import com.demo_assignment.model.News
import com.demo_assignment.news.MyNewsRepository
import com.demo_assignment.resources.StringProvider
import com.demo_assignment.rules.ViewModelCoroutineScopeRule
import com.demo_assignment.ui.error.CentralError
import com.demo_assignment.ui.progress.CentralProgressBar
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Visible
import com.demo_assignment.url_navigator.UrlNavigator
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
class MyNewsViewModelTest {

  @get:Rule
  val viewModelCoroutineScopeRule = ViewModelCoroutineScopeRule()

  private val newsItems: List<News> = listOf(
    News(
      title = "title1",
      imageURL = "imageUrl1",
      resourceName = "resourceName1",
      resourceURL = "resourceUrl1",
      newsLink = "newsLink1"
    ),
    News(
      title = "title2",
      imageURL = "imageUrl2",
      resourceName = "resourceName2",
      resourceURL = "resourceUrl2",
      newsLink = "newsLink2"
    )
  )

  @Mock
  lateinit var myNewsRepository: MyNewsRepository

  @Mock
  lateinit var logger: Logger
  @Mock
  lateinit var centralError: CentralError
  @Mock
  lateinit var centralProgressBar: CentralProgressBar
  @Mock
  lateinit var stringProvider: StringProvider<MyNewsString>
  @Mock
  lateinit var urlNavigator: UrlNavigator

  private lateinit var viewModel: MyNewsViewModel

  @Before
  fun setup() {
    `when`(centralError.errorButtonClickEvents).thenReturn(flow {  })
  }

  private fun setupMyNewsViewModel() {
    viewModel = MyNewsViewModel(
      repository = myNewsRepository,
      logger = logger,
      centralError = centralError,
      centralProgressBar = centralProgressBar,
      stringProvider = stringProvider,
      urlNavigator = urlNavigator
    )
  }

  @Test
  fun onInit_CentralProgressBarShowIsCalled() = runBlocking {
    setupMyNewsViewModel()
    verify(centralProgressBar).show()
  }

  @Test
  fun onInit_MyNewsLoadIsAttempted() = runBlocking {
    setupMyNewsViewModel()
    verify(myNewsRepository).getMyNews()
    Unit
  }

  @Test
  fun ifMyNewsLoadSuccessful_NewsItemsFlowEmitsTheSame() = runBlocking {
    `when`(myNewsRepository.getMyNews()).thenReturn(newsItems)
    setupMyNewsViewModel()

    val emittedNewsItems = viewModel.newsItems.first()
    assert(emittedNewsItems is Visible && emittedNewsItems.model == newsItems)
  }

  @Test
  fun ifMyNewsLoadSuccessful_CentralProgressBarHidden() = runBlocking {
    `when`(myNewsRepository.getMyNews()).thenReturn(newsItems)
    setupMyNewsViewModel()

    verify(centralProgressBar).hide()
  }

  @Test(expected = Exception::class)
  fun ifMyNewsLoadFailed_ViewModelCallsCentralErrorShow() = runBlocking {
    `when`(myNewsRepository.getMyNews()).thenThrow(Exception())
    setupMyNewsViewModel()

    verify(centralError).show(any(), any())
  }

  @Test(expected = Exception::class)
  fun ifMyNewsLoadFailed_CentralProgressBarHidden() = runBlocking {
    `when`(myNewsRepository.getMyNews()).thenThrow(Exception())
    setupMyNewsViewModel()

    verify(centralProgressBar).hide()
  }

  @Test
  fun onNewsItemClick_UrlNavigatorOpenShouldBeCalled() = runBlocking {
    setupMyNewsViewModel()
    viewModel.onNewsItemClick(newsItems[0])
    verify(urlNavigator).open(newsItems[0].newsLink)
  }
}