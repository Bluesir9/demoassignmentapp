package com.demo_assignment.ui.error

import com.demo_assignment.rules.ViewModelCoroutineScopeRule
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Hidden
import com.demo_assignment.ui.util.VisibleOrHiddenUiModel.Visible
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CentralErrorViewModelTest {

  @get:Rule
  val viewModelCoroutineScopeRule = ViewModelCoroutineScopeRule()

  private val message = "message"
  private val buttonText = "button_text"
  private lateinit var centralErrorViewModel: CentralErrorViewModel

  @Before
  fun setup() {
    centralErrorViewModel = CentralErrorViewModel()
  }

  @Test
  fun initiallyCentralErrorIsHidden() = runBlocking {
    val uiModel = centralErrorViewModel.centralErrorUiModels.first()
    assertTrue(uiModel is Hidden)
  }

  @Test
  fun onShow_VisibleUiModelIsEmitted() = runBlocking {
    val job = launch(start = CoroutineStart.UNDISPATCHED) {
      centralErrorViewModel.show(message, buttonText)
    }
    val uiModel = centralErrorViewModel.centralErrorUiModels.first()
    job.cancel()
    assertTrue(uiModel is Visible && verifyUiModel(uiModel.model))
  }

  private fun verifyUiModel(uiModel: CentralErrorUiModel): Boolean =
    uiModel.message == message && uiModel.buttonText == buttonText

  @Test
  fun onHide_HiddenUiModelIsEmitted() = runBlocking {
    val job = launch(start = CoroutineStart.UNDISPATCHED) {
      centralErrorViewModel.show(message, buttonText)
      centralErrorViewModel.hide()
    }
    val uiModel = centralErrorViewModel.centralErrorUiModels.first()
    job.cancel()
    assertTrue(uiModel is Hidden)
  }
}