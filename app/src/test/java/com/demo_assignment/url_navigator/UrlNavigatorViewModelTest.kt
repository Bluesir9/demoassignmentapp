package com.demo_assignment.url_navigator

import com.demo_assignment.rules.ViewModelCoroutineScopeRule
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UrlNavigatorViewModelTest {

  @get:Rule
  val viewModelCoroutineScopeRule = ViewModelCoroutineScopeRule()

  private val url = "https://google.com/"
  private lateinit var urlNavigatorViewModel: UrlNavigatorViewModel

  @Before
  fun setup() {
    urlNavigatorViewModel = UrlNavigatorViewModel()
  }

  @Test
  fun initiallyUrlStreamHasNullValue() = runBlocking {
    assertTrue(urlNavigatorViewModel.urlStream.first() == null)
  }

  @Test
  fun onOpenUrl_UrlStreamShouldHaveSameUrlAvailable() = runBlocking {
    val job = launch(start = CoroutineStart.UNDISPATCHED) {
      urlNavigatorViewModel.open(url)
    }

    val checkUrl = urlNavigatorViewModel.urlStream.first()
    job.cancel()
    assertTrue(url == checkUrl)
  }
}